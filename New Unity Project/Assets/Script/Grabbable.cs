﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabbable : MonoBehaviour
{
    private Player grabber;
    private Rigidbody rb;

    private void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }
    public bool Grab(Player newGrabber)
    {
        if(!grabber)
        {
            transform.parent = newGrabber.transform;
            grabber = newGrabber;
            rb.isKinematic = true;
            GetComponent<Collider>().isTrigger = true;
            return true;
        }
        return false;
    }
    
    public bool Drop()
    {
        if (grabber)
        {
            transform.parent = null;
            grabber = null;
            rb.isKinematic = false;
            GetComponent<Collider>().isTrigger = false;
            return true;
        }

        return false;
    }
}
