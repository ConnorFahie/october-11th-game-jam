﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonActor : MonoBehaviour
{
    public Light buttonLight;
    public void onButtonPress()
    {
        buttonLight.color = Color.green;
    }

    public void onButtonUp()
    {
        buttonLight.color = Color.red;
    }
}
