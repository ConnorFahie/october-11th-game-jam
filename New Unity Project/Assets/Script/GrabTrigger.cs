﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabTrigger : MonoBehaviour
{
    public Grabbable grabCandidate;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Grabbable>()) grabCandidate = other.gameObject.GetComponent<Grabbable>();
    }
    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.GetComponent<Grabbable>() == grabCandidate) grabCandidate = null;
    }
}
