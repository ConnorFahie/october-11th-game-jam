﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : PowerSource
{
    public Grabbable buttonPresser;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Grabbable>())
        {
            buttonPresser = other.gameObject.GetComponent<Grabbable>();
            SetPowered(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (buttonPresser = other.gameObject.GetComponent<Grabbable>())
        {
            buttonPresser = null;
            SetPowered(false);
        }
    }

}
