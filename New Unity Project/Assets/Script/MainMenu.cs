﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void button1()
    {
        SceneManager.LoadScene("Main");
    }

    public void button2()
    {
        Application.Quit();
    }
}
