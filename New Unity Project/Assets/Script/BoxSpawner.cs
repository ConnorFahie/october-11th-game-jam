﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawner : MonoBehaviour
{
    public GameObject spawnObject;
    private float spawnTimer = 5.0f;
    void Update()
    {
        if (spawnTimer <= 0)
        {
            GameObject newSpawn = Instantiate(spawnObject, transform.position, transform.rotation);
            newSpawn.GetComponent<Rigidbody>().velocity = transform.right * 13;
            spawnTimer = 5.0f;
        }
        spawnTimer -= Time.deltaTime;
    }
}
