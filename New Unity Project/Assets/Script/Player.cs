﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public Rigidbody rb;
    public GrabTrigger grabArea;
    public Transform spawnPoint;
    public Animator anim;
    public float speed = 10;
    public float dashSpeed = 20;
    public float zSpeed = 10;
    public float maxSpeed = 30;
    public float jumpForce = 30;
    public bool enableZ = false;

    private bool returnedToZero = true;
    private float lastTapTime = 0;
    private float doubleTapTiming = 0.1f;
    private bool lastTappedRight = true;
    private bool currentTappingRight = true;
    private int dashing = 0;
    private float dashTimer = 0.2f;

    private Grabbable heldObject = null;

    // Start is called before the first frame update
    void Start()
    {
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
    }

    bool jumpQueued = false;
    bool interactQueued = false;

    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
            jumpQueued = true;

        if (Input.GetButtonDown("Activate"))
            interactQueued = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        anim.SetFloat("Velocity", Mathf.Abs(rb.velocity.z));
        anim.SetFloat("FallVelocity", rb.velocity.y);

        if (Input.GetAxis("Horizontal") != 0 && dashing == 0)
        {
            if (Input.GetAxis("Horizontal") > 0)
            {
                transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                if (rb.velocity.magnitude < maxSpeed) rb.AddForce(transform.right * speed);
                if (returnedToZero)
                {
                    currentTappingRight = true;

                    if (lastTappedRight && lastTapTime < doubleTapTiming)
                    {
                        dashing = 1; //dashing right
                        dashTimer = 0.2f;
                    }
                    returnedToZero = false;
                }
            }
            else
            {
                transform.localScale = new Vector3(-0.3f, 0.3f, 0.3f);
                if (rb.velocity.magnitude < maxSpeed) rb.AddForce(transform.right * -speed);
                if (returnedToZero)
                {
                    currentTappingRight = false;

                    if (!lastTappedRight && lastTapTime < doubleTapTiming)
                    {
                        dashing = -1; //dashing left
                        dashTimer = 0.2f;
                    }
                    returnedToZero = false;
                }
            }

            if (Input.GetAxis("Horizontal") != 0)
            {
                lastTappedRight = currentTappingRight;
                lastTapTime = 0;
            }
        }
        else
        {
            returnedToZero = true;
            if(dashing == -1 && dashTimer > 0)
            {
                rb.velocity = transform.right * -dashSpeed;
            }

            if (dashing == 1 && dashTimer > 0)
            {
                rb.velocity = transform.right * dashSpeed;
            }
            dashTimer -= Time.deltaTime;
            if (dashTimer < 0)
            {
                dashing = 0;
                rb.AddForce( -rb.velocity * 0.8f );
            }
        }

        if (Input.GetAxis("Vertical") != 0)
        {
            if (Input.GetAxis("Vertical") > 0) rb.AddForce(transform.forward * zSpeed);
            else rb.AddForce(transform.forward * -zSpeed);
        }

        if(jumpQueued)
        {
            if (rb.velocity.y <= 0.001 && rb.velocity.y >= -0.001) rb.AddForce(new Vector3(0, jumpForce, 0));
            jumpQueued = false;
        }

        if(interactQueued)
        {
            if (heldObject)
            {
                heldObject.Drop();
                heldObject = null;
            }
            else if (!heldObject && grabArea.grabCandidate)
            {
                if (grabArea.grabCandidate.Grab(GetComponent<Player>())) heldObject = grabArea.grabCandidate;
            }

            interactQueued = false;
        }

        Debug.Log(rb.velocity.y);
        lastTapTime += Time.deltaTime;
    }

    public void Die()
    {
        transform.position = Vector3.zero;
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (anim == null)
            anim = GetComponent<Animator>();
    }
#endif
}
