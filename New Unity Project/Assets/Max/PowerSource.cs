﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PowerSource : MonoBehaviour
{
    [SerializeField] private bool _isPowered;
    public bool IsPowered { get { return _isPowered; } }

    public PowerEvent OnPoweredStart;
    public PowerEvent WhilePowered;
    public PowerEvent OnPoweredStop;

    public void SetPowered(bool value)
    {
        if(value != _isPowered)
        {
            _isPowered = value;
            if (_isPowered)
                OnPoweredStart.Invoke(this);
            else
                OnPoweredStop.Invoke(this);
        }
    }

    protected virtual void FixedUpdate()
    {
        if(_isPowered)
        {
            WhilePowered.Invoke(this);
        }
    }
}

[System.Serializable]
public class PowerEvent : UnityEvent<PowerSource> { }