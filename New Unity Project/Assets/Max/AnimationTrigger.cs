﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTrigger : MonoBehaviour
{
    [SerializeField] protected Animator _anim;

    public void SetBoolTrue(string paramName)
    {
        _anim.SetBool(paramName, true);
    }
    public void SetBoolFalse(string paramName)
    {
        _anim.SetBool(paramName, false);
    }
    public void ToggleBool(string paramName)
    {
        _anim.SetBool(paramName, !_anim.GetBool(paramName));
    }
    public void SetTrigger(string paramName)
    {
        _anim.SetTrigger(paramName);
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (_anim == null)
            _anim = GetComponent<Animator>();
    }
#endif
}
