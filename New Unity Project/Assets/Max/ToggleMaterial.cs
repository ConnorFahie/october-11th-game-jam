﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleMaterial : MonoBehaviour
{
    [SerializeField] protected Renderer _renderer;
    [SerializeField] protected Material _defaultMaterial;
    [SerializeField] protected Material _poweredMaterial;

    public void SetMaterial(PowerSource source)
    {
        if(source.IsPowered)
        {
            _renderer.material = _poweredMaterial;
        }
        else
        {
            _renderer.material = _defaultMaterial;
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (_renderer == null)
        {
            _renderer = GetComponent<Renderer>();
        }

        if(_renderer)
        {
            _defaultMaterial = _renderer.sharedMaterial;
        }
    }
#endif
}
