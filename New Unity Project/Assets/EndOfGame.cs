﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class EndOfGame : MonoBehaviour
{
    public float timeTilReturn = 5.0f;
    public int menuBuildIndex = 0;
    public UnityEvent OnTriggerEnd;

    private bool hasEnded = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !hasEnded)
        {
            hasEnded = true;
            OnTriggerEnd.Invoke();
            StartCoroutine(ReturnToMainAfter());
        }
    }

    IEnumerator ReturnToMainAfter()
    {
        yield return new WaitForSeconds(timeTilReturn);
        SceneManager.LoadScene(menuBuildIndex);
    }
}
